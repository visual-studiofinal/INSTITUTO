//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace INSTITUTO.BE
{
    using System;
    using System.Collections.Generic;
    
    public partial class Docente
    {
        public Docente()
        {
            this.Curso_docente = new HashSet<Curso_docente>();
            this.Seccion = new HashSet<Seccion>();
        }
    
        public string Cod_docente { get; set; }
        public string nombre { get; set; }
        public string ap_paterno { get; set; }
        public string ap_materno { get; set; }
        public string DNI { get; set; }
        public Nullable<System.DateTime> fecha_reg { get; set; }
        public string email { get; set; }
        public string celular { get; set; }
        public string estado { get; set; }
    
        public virtual ICollection<Curso_docente> Curso_docente { get; set; }
        public virtual ICollection<Seccion> Seccion { get; set; }
    }
}
