﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INSTITUTO.BE
{
    public class cursoxcodigo
    {

        public string IdCuenta { get; set; }
        public string Cod_semestre { get; set; }
        public string Cod_carrera { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public string Estado { get; set; }
        
    }
}
