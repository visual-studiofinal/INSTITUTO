﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using INSTITUTO.BE;
using System.Data.Entity;
using System.Data.Objects.SqlClient;
using System.Data.Objects;


namespace INSTITUTO.DA
{
  public  class cursoxcodigoDA
    {
        public List<cursoxcodigo> GetCurso(string codigo)
        {
            try
            {
                ObjectResult<sp_curcod_Result> list;
                List<cursoxcodigo> listaFnal = new List<cursoxcodigo>();
                using (var dbcontext = new BD_INSTITUTOEntities1())
                {
                    list = dbcontext.sp_curcod(codigo);
                    System.Collections.IEnumerator enumerator = null;
                    enumerator = ((System.Collections.IEnumerable)list).GetEnumerator();// FALTO ESTA LINEA
                    cursoxcodigo cursoxcodt;
                    while (enumerator.MoveNext())
                    {
                        cursoxcodt = new cursoxcodigo();
                        sp_curcod_Result sp_curcod =
                            (sp_curcod_Result)enumerator.Current;
                      
                       
                        cursoxcodt.Cod_semestre = sp_curcod.Cod_semestre;
                        cursoxcodt.Cod_carrera = sp_curcod.Cod_carrera;
                         cursoxcodt.FechaRegistro =sp_curcod.fecha_reg;
                        cursoxcodt.Estado = sp_curcod.estado;

                        listaFnal.Add(cursoxcodt);
                    }
                    return listaFnal;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
